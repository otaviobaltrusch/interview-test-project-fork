﻿namespace SampleSite.DbContext
{
    using Microsoft.EntityFrameworkCore;
    using SampleSite.Models.Entities;

    public class SampleSiteDBContext : DbContext
    {
        public SampleSiteDBContext(DbContextOptions<SampleSiteDBContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
                .HasMany(u => u.Addresses);
        }

        public DbSet<Product> Products { get; set; }
        public DbSet<User> Users { get; set; }
    }
}
