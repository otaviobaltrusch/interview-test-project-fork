﻿namespace SampleSite.Models.Forms
{
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;

    public class AddressForm
    {
        [DisplayName("Address 1")]
        [Required]
        public string Address1 { get; set; }

        [DisplayName("Address 2")]
        public string Address2 { get; set; }

        [DisplayName("Postcode")]
        [Required]
        public string PostCode { get; set; }

        [DisplayName("Cit")]
        [Required]
        public string City { get; set; }
    }
}
