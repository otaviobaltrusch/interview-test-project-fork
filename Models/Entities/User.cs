﻿using System.Collections.Generic;

namespace SampleSite.Models.Entities
{
    public class User : BaseEntity
    {
        public User()
        {
            this.Addresses = new List<Address>();
        }

        public string UserName { get; set; }

        public string Email { get; set; }

        public int Age { get; set; }

        public virtual ICollection<Address> Addresses { get; set; }
    }
}
